import 'package:flutter/material.dart';

    class PageIndex extends StatelessWidget {
      const PageIndex({super.key});

      @override
      Widget build(BuildContext context) {
        return Scaffold(
          appBar: AppBar(
            title: const Text(
              '내 사랑 카라칼'
            ),
          ),
          body: SingleChildScrollView(
            child: Column(
              children: [
                Image.asset('assets/caracal1.jpg',
                width: 250,
                  height: 250,
                  fit: BoxFit.fill,
                ),
                Container(
                  child: const Column(
                    children: [
                      Text(
                          '카라칼을 소개합니다',
                      style: TextStyle(
                        letterSpacing: 2.0,
                      ),
                      ),
                      Text('카라칼은 귀여워요. 하지만 하악질이 심해요'),
                    ],
                  ),
                ),
                Container(
                  child:
                  Column(
                    children: [
                      Row(
                        children: [
                          Image.asset('assets/caracal2.jpg',
                            width: 200,
                            height: 200,
                            fit: BoxFit.fill,
                          ),
                          Image.asset('assets/caracal3.jpg',
                            width: 200,
                            height: 200,
                            fit: BoxFit.fill,
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Image.asset('assets/caracal4.jpg',
                            width: 200,
                            height: 200,
                            fit: BoxFit.fill,
                          ),
                          Image.asset('assets/caracal5.jpg',
                            width: 200,
                            height: 200,
                            fit: BoxFit.fill,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      }
    }
